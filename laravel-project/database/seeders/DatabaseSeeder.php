<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();

        DB::table('tb_terminalATM')->insert([
            'NomorTerminal' => 'SV0001', 
            'Merk' => 'HITACHI'
        ]);
        DB::table('tb_terminalATM')->insert([
            'NomorTerminal' => 'SV0002', 
            'Merk' => 'HITACHI'
        ]);
        DB::table('tb_terminalATM')->insert([
            'NomorTerminal' => 'SV0003', 
            'Merk' => 'HYOSUNG'
        ]);
        DB::table('tb_terminalATM')->insert([
            'NomorTerminal' => 'SV0004', 
            'Merk' => 'HYOSUNG'
        ]);
        DB::table('tb_terminalATM')->insert([
            'NomorTerminal' => 'SV0005', 
            'Merk' => 'HYOSUNG'
        ]);

        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0001', 
            'NomorKartu' => '55679293945',
            'Amount' => '100000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0001', 
            'NomorKartu' => '55678475829',
            'Amount' => '150000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0001', 
            'NomorKartu' => '55678475829',
            'Amount' => '500000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0001', 
            'NomorKartu' => '55678475029',
            'Amount' => '1000000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0001', 
            'NomorKartu' => '55678475333',
            'Amount' => '1250000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0003', 
            'NomorKartu' => '77908473632',
            'Amount' => '600000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0003', 
            'NomorKartu' => '77908444632',
            'Amount' => '300000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0005', 
            'NomorKartu' => '77908441132',
            'Amount' => '350000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0005', 
            'NomorKartu' => '77908221132',
            'Amount' => '100000'
        ]);
        DB::table('tb_transaksiATM')->insert([
            'Terminal' => 'SV0005', 
            'NomorKartu' => '77908487632',
            'Amount' => '50000'
        ]);

        DB::table('tb_issuer')->insert([
            'Prefix' => '5567', 
            'Rank' => 'A',
        ]);
        DB::table('tb_issuer')->insert([
            'Prefix' => '5520', 
            'Rank' => 'B',
        ]);
        DB::table('tb_issuer')->insert([
            'Prefix' => '7790', 
            'Rank' => 'C',
        ]);
        DB::table('tb_issuer')->insert([
            'Prefix' => '8890', 
            'Rank' => 'D',
        ]);

        
    }
}
