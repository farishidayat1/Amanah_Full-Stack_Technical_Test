<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QrController extends Controller
{
    public function index()
    {
        $uniqueCode = 'MN-DPK-07-2022-12';
        $url = route('show-popup', ['code' => $uniqueCode]);

        return view('qr-code', [
            'url' => $url
        ]);
    }

    public function popup(Request $request)
    {
        $code = explode('-', $request->code);
        
        $item = '';
        if($code[0] == 'MN') {
            $item = 'Monitor';
        }

        $area = '';
        if($code[1] == 'DPK') {
            $area = 'Depok';
        }

        $month = '';
        if($code[2] == '07') {
            $month = date('F', $code[2]);
        }

        $year = $code[3];
        $query = 12;

        $contentPopup = $item." area ".$area." pembelian "." bulan ".$month." tahun ".$year." ke ".$query;

        return view('popup', [
            'contentPopup' => $contentPopup
        ]);
    }
}
