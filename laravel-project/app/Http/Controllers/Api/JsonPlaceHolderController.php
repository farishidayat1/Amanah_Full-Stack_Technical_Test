<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class JsonPlaceHolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = Http::get('https://jsonplaceholder.typicode.com/posts');

        return response()->json($res->json());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = Http::post('https://jsonplaceholder.typicode.com/posts', [
            'title' => $request->title,
            'body' => $request->body,
            'userId' => $request->userId
        ]);

        return response()->json($res->json());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = Http::get('https://jsonplaceholder.typicode.com/posts/'.$id);

        return response()->json($res->json());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = Http::put('https://jsonplaceholder.typicode.com/posts/'.$id, [
            'id' => $request->id,
            'title' => $request->title,
            'body' => $request->body,
            'userId' => $request->userId
        ]);

        return response()->json($res->json());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Http::delete('https://jsonplaceholder.typicode.com/posts/'.$id);

        return response()->json($res->json());
    }
}
