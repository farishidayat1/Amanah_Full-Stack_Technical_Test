<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\QrController;
use App\Http\Controllers\TiketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/view-qr', [QrController::class, 'index']);
Route::get('/show-popup', [QrController::class, 'popup'])->name('show-popup');
Route::get('tickets', [TiketController::class, 'index']);

// Route::get('/', function () {
//     return view('welcome');
// });
