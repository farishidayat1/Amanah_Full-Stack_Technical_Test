<?php

use App\Http\Controllers\Api\JsonPlaceHolderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('json-placeholder')->group(function () {
    Route::get('get', [JsonPlaceHolderController::class, 'index']);
    Route::post('store', [JsonPlaceHolderController::class, 'store']);
    Route::get('show/{id}', [JsonPlaceHolderController::class, 'show']);
    Route::put('update', [JsonPlaceHolderController::class, 'update']);
    Route::delete('delete', [JsonPlaceHolderController::class, 'delete']);
});