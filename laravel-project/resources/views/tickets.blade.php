<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body style="background-color: grey;">

        <div class="card" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
            <div class="card-body">
                <p>RIMOTE</p>
                <p>SALDO QC TREG 2</p>
                <p>15 Juni 2022 - 22 Juni 2022</p>
            </div>
        </div>

        <div class="card" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
            <div class="card-body">
                <table class="table table-striped">
                    @foreach($dateArray as $date)
                        <thead>
                            <tr>
                                <th colspan="1">Witel</th>
                                <th scope="col">
                                    {{ $date }}
                                    <tr>
                                        <th scope="col">TIKET H-1</th>
                                        <th scope="col">OPEN</th>
                                    </tr>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $startCarbon = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->startOfDay()->format('Y-m-d H:i:s');
                                $endCarbon = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->endOfDay()->format('Y-m-d H:i:s');

                                $ticketH1 = DB::table('measuring_indications')->whereBetween('created_at', [$startCarbon, $endCarbon])->count();
                                $open = DB::table('measuring_indications')->whereBetween('created_at', [$startCarbon, $endCarbon])->where('status_indicator_data', 'open')->count();
                            @endphp
                            <tr>
                                <td>{{ $ticketH1 }}</td>
                                <td>{{ $open }} </td>
                            </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </body>
</html>