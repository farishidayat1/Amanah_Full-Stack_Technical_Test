let error;

if(!OK(Run1())) {
   error = Run1Err;
} else if(!OK(Run2())) {
    error = Run2Err;
} else if(!OK(Run3())) {
    error = Run3Err;
} else if(!OK(Run4())) {
    error = Run4Err;
}

return error;